'''
Name:Joshua White

'''
def answer(names):
    letterInAlpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                     'u', 'v', 'w', 'x', 'y', 'z']
    filler = 0
    valOfLetters = {}

    while len(letterInAlpha) > filler:
        valOfLetters.update({letterInAlpha[filler]: filler + 1})
        filler += 1
    currVal = 0
    prevVal = 0
    size = 0
    prevKey = ''
    curKey = ''
    isLex = False
    numNonSortList = []
    numSortList = []
    index = 0
    for posName in names:
        size = valCheck(str(posName), valOfLetters)
        x = WordValue()
        x.value = size
        x.word = posName
        numSortList.append( x )
        index += 1
    #sort objects by the numeric value
    selectionSort(numSortList)
    flag = False
    #go through the loop until list with same values are lexicographic
    while flag == False:
        idx = 0
        flag = True
        while idx < len(numSortList):
            prev = numSortList[idx-1]
            curr = numSortList[idx]
            if idx != 0 and curr.value == prev.value:
                tf = lexCheck(curr.word, prev.word)
                if tf == False:
                    tmp = numSortList[idx]
                    numSortList[idx] = numSortList[idx-1]
                    numSortList[idx-1] = tmp
                    flag = False

            idx += 1
    #holds words in order instead of object
    orderedArray=[]
    for key in numSortList:
       orderedArray.append(key.word)
    fIdx = 0
    revIdx = len(orderedArray) - 1
    regList = []
    #swaps list into reverse order
    while fIdx < len(orderedArray):
        regList.append(orderedArray[revIdx])
        print regList[fIdx]
        fIdx += 1
        revIdx -= 1
    return regList

#checks values of each letter to the pre made dictionary
def valCheck(singleName, letterInAlpha):
    totalSize = 0
    idx = 0
    letter = ''
    while idx < len(singleName):
        letter = singleName[idx]
        totalSize += letterInAlpha.get(letter)
        idx += 1
    return totalSize


#checks if items in list are lexicographic
def lexCheck(singleName, sameValName):
    if singleName > sameValName:
        return True
    else:
        return False

#sorts objects in array by value
def selectionSort(wordVal):
   for fillslot in range(len(wordVal)-1,0,-1):
       positionOfMax=0
       for location in range(1,fillslot+1):
           if wordVal[location].value>wordVal[positionOfMax].value:
               positionOfMax = location
       temp = wordVal[fillslot]
       wordVal[fillslot] = wordVal[positionOfMax]
       wordVal[positionOfMax] = temp

#class for storing data about name
class WordValue:
    value = 0
    word = ''




nam = ["a", "b", "c", "d", "e", "f","g", "h","hh","bb","dd"]
answer(nam)
